# README #

Instalacion para los agentes de [shellhub.io](https://shellhub.io) y [packetriot.com](https://packetriot.com)
en un [emonpi](https://shop.openenergymonitor.com/emonpi) permitiendo el acceso remoto sin habiltar puertos en la red del cliente 

### Agentes ###

* El agente de **Shellhub** permite acceder via SSH
* El agente de **Pascketriot** permite acceder via HTTP/S y TCP tunnels

### Shellhub agent ###

Docker debe estar instalado. De lo contrariio se instalara

    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh

En un contenedor en ejecución, para cambiar la política de 
reinicio, usar el comando de actualización:

    sudo docker update --restart unless-stopped container_id
    
### Shellhub login

Ingresar a la cuenta de Shellhub [https://cloud.shellhub.io/login](https://cloud.shellhub.io/login)

![shellhub_login](https://bitbucket.org/mmoncalvo/emonpi/raw/4f70869e11cd3ee3966c3b8583a6ebe95de6b620/Images/shellhub_login.png)

### Shellhub devices

Instalar el agente ShellHub con el script de instalación automático.
Funciona con todas las distribuciones de Linux que tienen Docker instalado y configurado. [https://cloud.shellhub.io/devices](https://cloud.shellhub.io/devices)

![shellhub_devices](https://bitbucket.org/mmoncalvo/emonpi/raw/17b9eee82459f1b92dd97fc9da3edbd01a10bfae/Images/shellhub_devices.png)

### Packetriot agent ###

Instalació en Linux ARMv7 (Raspberry Pi 1/2/3/zero and similar SBC)

    wget https://download.packetriot.com/linux/debian/buster/stable/non-free/binary-armhf/pktriot-0.10.8.armhf.deb
    sudo dpkg -i pktriot-0.10.8.armhf.deb

configurar:
    
    pktriot configure
    
Correr el agente con las configuraciones:

    pktriot start
    
Definir el directorio raiz:
    
    pktriot http 80 --webroot /var/www/emoncms
    
### Packetriot login

Ingresar a la cuenta de Packetriot [https://packetriot.com/login](https://packetriot.com/login)

![packetriot_login](https://bitbucket.org/mmoncalvo/emonpi/raw/e361f492aa72c7c973a32fb593ef0b65700a4f91/Images/packetriot_login.png)


### Packetriot tunnels

En la cuenta de Packetriot, ingresar a la sección Tunnels  [https://packetriot.com/tunnels](https://packetriot.com/tunnels)

![packetriot_tunnels](https://bitbucket.org/mmoncalvo/emonpi/raw/5f5a3f9ee7e3d4bd215707229ab541590c8e74cd/Images/packetriot_tunnels.png)
